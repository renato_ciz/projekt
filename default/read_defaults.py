import pandas as pd
import json
from urllib.request import urlopen

url = "https://gitlab.com/renato_ciz/projekt/-/raw/main/texts.json"
response = urlopen(url)
texts = json.loads(response.read())
data_basic = pd.read_csv("https://gitlab.com/renato_ciz/projekt/-/raw/main/basic.csv")
